const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const meanSchema = new Schema({
  userName: {type: String,
    index: true,
    required: true,
    unique: true,
    lowercase: true,
    maxlength: 50},
  password: {type: String,
    required: true},
  firstName: {type: String,
    required: false,
    lowercase: true,
    maxlength: 50},
  lastName: { type: String,
    required: false,
    lowercase: true,
    maxlength: 50},
  profileImage: {type: Buffer,
    required: false},
  interest: { type: String,
    required: false,
    lowercase: true,
    maxlength: 2000}
}, {collection: 'profiles'});


// Virtual for user's URL
meanSchema
    .virtual('url')
    .get(function () {
      return '/profile/' + this._id;
    });

module.exports = mongoose.model('User', meanSchema);