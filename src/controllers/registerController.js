// Get the required model
const User = require('../models/mean_schema');
const {body, validationResult} = require('express-validator/check');
const {sanitizeBody} = require('express-validator/filter');
const bcrypt = require('bcrypt');

// return the register page
exports.index = function (req, res) {
  res.render('register', {title: 'Register-Fresh Start'});
};

exports.validate = (method) => {
  switch (method) {
    case 'createUser': {
      return [
        body('fName', 'First name required').exists(),
        body('lName', 'Last name required').exists(),
        body('email', 'Invalid email').exists().isEmail(),
        body('password', 'Password Invalid').exists()
      ]
    }
  }
};

//Process post request
exports.postUser = (req, res, next) => {
  //Get the errors of the form
  const errors = validationResult(req);
  bcrypt.genSalt(10, function (err, salt) {
    bcrypt.hash(req.body.password, salt, function (err, hash) {
      const newUser = new User({
        userName: req.body.email,
        password: hash,
        firstName: req.body.fName,
        lastName: req.body.lName,
        interest: "Not sure yet"

      });
      if (!errors.isEmpty()) {
        res.render('register.ejs', {title: 'Register-Fresh Start', data: req.body});
      } else {
        // The data is good.
        //Check to make sure it doesn't already exists
        User.findOne({'userName': req.body.email})
            .exec(function (err, found_userName) {
              if (err) {
                return next(err);
              }
              if (found_userName) {
                //Name already exist
              } else {
                newUser.save(function (err) {
                  if (err) {
                    return next(err);
                  }
                  //user was saved redirct to login page
                  res.redirect('/login');
                })
              }
            });
      }
    });
  });

};