// Get the required model
const User = require('../models/mean_schema');

// return the profile page
exports.index = function(req, res) {
  res.render('profile', {title: 'Profile-Fresh Start'});
};

exports.userProfile = function (req, res, next) {
  console.log("user id:" + req.session.userId);
  User.findById(req.session.userId)
      .exec(function (error, user) {
        if (error) {
          return next(error);
        } else {
          if (user === null) {
            const err = new Error('Not authorized!');
            err.status = 400;
            return next(err);
          } else {
            res.render('profile', {
              title: 'Profile-Fresh Start',
              interest: user.interest,
              firstName: user.firstName,
              lastName: user.lastName
            })
          }
        }
      })
};

//Logout function
exports.userLogout = function (req, res, next) {
  console.log('user logout called');
  if (req.session) {
    //delete session
    req.session.destroy(function (err) {
      if (err) {
        return next(err);
      } else {
        res.redirect('/home');
      }
    })
  }
};