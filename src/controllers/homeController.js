//return the home page
exports.home = function (req, res) {
  res.render('home.ejs', {title: 'Fresh Start'});
};