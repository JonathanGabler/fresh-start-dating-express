// Get the required model
const User = require('../models/mean_schema');
const {body, validationResult} = require('express-validator/check');
const bcrypt = require('bcrypt');

// return the login page
exports.index = function(req, res) {
  res.render('login', {title: 'Login-Fresh Start'});
};

exports.validate = (method) => {
  switch (method) {
    case 'loginUser': {
      return [
        body('email', 'Invalid email').exists().isEmail(),
        body('password', 'Password Invalid').exists()
      ]
    }
  }
};

//Process post request
exports.loginUser = (req, res, next) => {
  //Get the errors of the form
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.render('register.ejs', {title: 'Register-Fresh Start', data: req.body});
  } else {
    // The data is good.
    //Check to make sure it doesn't already exists
    User.findOne({'userName': req.body.email})
        .exec(function (err, found_userName) {
          if (err) {
            return next(err);
          }
          if (found_userName) {
            //Name is found
            const userPass = found_userName.password;
            bcrypt.compare(req.body.password, userPass, function (err, result) {
              if (result === true) {
                console.log('Passwords Matched');
                req.session.userId = found_userName._id;
                res.redirect('/profile');
              } else {
                console.log('Incorrect Password');
                res.redirect('/');
              }
            })
          } else {
            res.redirect('/home');
          }
        });
  }
};

