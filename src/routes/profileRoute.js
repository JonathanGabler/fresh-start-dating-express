const express = require('express');
const profileRouter = express.Router();

// Controller modeules
const profile_controller = require('../../src/controllers/profileController');

// GET app home page.
profileRouter.get('/', profile_controller.userProfile);

profileRouter.get('/profile', profile_controller.userProfile);

profileRouter.get('/logout', profile_controller.userLogout);

module.exports = profileRouter;