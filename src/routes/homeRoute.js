const express = require('express');
const homeRouter = express.Router();

// Controller modeules
const home_controller = require('../../src/controllers/homeController.js');

// GET app home page.
homeRouter.get('/', home_controller.home);

module.exports = homeRouter;