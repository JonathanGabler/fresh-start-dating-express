const express = require('express');
const registerRouter = express.Router();

// Controller modeules
const register_controller = require('../../src/controllers/registerController');

// GET app home page.
registerRouter.get('/', register_controller.index);

// POST method on creating new user
registerRouter.post('/submit', register_controller.validate('createUser'), register_controller.postUser);

module.exports = registerRouter;