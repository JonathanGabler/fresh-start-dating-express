const express = require('express');
const loginRouter = express.Router();

// Controller modeules
const login_controller = require('../../src/controllers/loginController');

// GET app home page.
loginRouter.get('/', login_controller.index);

// POST method on creating new user
loginRouter.post('/submit', login_controller.validate('loginUser'), login_controller.loginUser);

module.exports = loginRouter;