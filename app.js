const express = require('express');
const chalk = require('chalk').default;
const debug = require('debug')('app');
const morganLogger = require('morgan');
const path = require('path');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const session = require('express-session');
const MongoStore = require('connect-mongo')(session);

// Setup Express
const app = express();

// Set up mongoose connection
const mongoDB = 'mongodb://localhost/mean';
const MONGO_USERNAME = 'matchMaker';
const MONGO_PASSWORD = 'p@ssw0rd';
mongoose.connect(mongoDB, {
  auth: {
    user: MONGO_USERNAME,
    password: MONGO_PASSWORD,
  },
  useNewUrlParser: true,
});
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

// Test the connection and report success if conected
mongoose.connection.once('open', () => {
  debug(chalk.cyan('Mongoose Connection Open'));
});

//use sessions for tracking login
app.use(session({
  secret: 'Client Side Course',
  resave: true,
  saveUninitialized: false,
  store: new MongoStore({
    mongooseConnection: db
  })
}));

// Grab the port number from env
const port = process.env.PORT || 8080;

// Setting up router
const homeRouter = require('./src/routes/homeRoute');
const loginRouter = require('./src/routes/loginRoute');
const profileRouter = require('./src/routes/profileRoute');
const registerRouter = require('./src/routes/registerRoute');

// Set morgan log level
app.use(morganLogger('tiny'));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

// Set the public dir
app.use(express.static(path.join(__dirname, '/public/')));
app.use('/css', express.static(path.join(__dirname, 'node_modules/bootstrap/dist/css')));
app.use('/js', express.static(path.join(__dirname, 'node_modules/bootstrap/dist/js')));
app.use('/js', express.static(path.join(__dirname, 'node_modules/jquery/dist')));
app.use('/js', express.static(path.join(__dirname, 'node_modules/popper.js/dist/umd')));
app.set('views', './src/views');
app.set('view engine', 'ejs');

// Route Calls
app.use('/', homeRouter);
app.use('/home', homeRouter);
app.use('/login', loginRouter);
app.use('/profile', profileRouter);
app.use('/register', registerRouter);

// Set listening port
app.listen(port, () => {
  debug(`listening on port ${chalk.green(port)}`);
});
